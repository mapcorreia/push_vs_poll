const index = require("./index");
var app = require("express")();
let currentOdd = 0;
let updatedOdd = 0;
const cors = require("cors");

const serverPort = 5000;

var httpServer = require("http").Server(app);

var io = require("socket.io")(httpServer, {
  pingInterval: 10000, // how many ms before sending a new ping packet
  pingTimeout: 5000, // how many ms without a ping packet to consider the connection closed
  cors: { origin: "http://localhost:3000" },
  methods: ["GET"],
});

app.use(cors());

// By default, a long-polling connection is established first, and then upgraded to WebSocket if possible

// Server initializer
httpServer.listen(serverPort, () => {
  valueGenerator();
  console.log(`Listening on  port ${serverPort}`);
});

// Socket Connection
io.on("connection", (socket) => {
  console.log("Connection Established");
  socket.on("disconnect", () => {
    console.log("Client Disconnected");
  });
});

const generateRandomNumber = (min, max) => {
  return (Math.random() * (max - min) + min).toFixed(2);
};

const valueGenerator = () => {
  let interval = generateRandomNumber(3000, 8000);
  updatedOdd = generateRandomNumber(1, 6);

  if (updatedOdd !== currentOdd) {
    console.log("Emiting new Odd:", updatedOdd);
    io.emit("oddUpdate", updatedOdd);
    currentOdd = updatedOdd;
  }

  setTimeout(valueGenerator, interval);
};

// API for polling
app.get(`/getOddUpdate`, cors(), (req, res) => {
  console.log("Poll Request Received. Odd Response =", updatedOdd);
  res.header("Access-Control-Allow-Origin", "*");
  res.send(updatedOdd + "\n");
});
