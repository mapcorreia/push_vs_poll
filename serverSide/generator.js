const generateRandomNumber = (min, max) => {
  return (Math.random() * (max - min) + min).toFixed(2);
};

const valueGenerator = () => {
  let interval = generateRandomNumber(1000, 8000);

  console.log("Interval", interval);
  console.log(generateRandomNumber(1, 6));

  setTimeout(valueGenerator, interval);
};
