import "./App.css";
import React, { useCallback, useEffect, useRef, useState } from "react";
import axios from "axios";

let io = require("socket.io-client")("http://localhost:5000");

let polledOdd;
let pollCounter = 0;
let timesOddPolledChanged = 0;
const ANIMATION_DURATION = 700;

export const App = () => {
  const [currentPollValue, setPollingValue] = useState(0);
  const [currentPushValue, setPushValue] = useState(0);
  const [pollAnimation, setPollAnimation] = useState("");
  const [pushAnimation, setPushAnimation] = useState("");

  const timeout = useRef();

  const blinkOnChange = useCallback(
    (isPoll) => {
      let isAnimating = false;

      if (timeout.current) {
        clearTimeout(timeout.current);
      }
      if (isPoll) {
        setPollAnimation("flashAnimation");
      } else {
        setPushAnimation("flashAnimation");
      }
      timeout.current = setTimeout(() => {
        if (!isAnimating) {
          setPushAnimation("");
          setPollAnimation("");
        }
      }, ANIMATION_DURATION);
      return () => {
        isAnimating = true;
      };
    },
    [setPushAnimation, setPollAnimation]
  );

  // Start Polling every second
  useEffect(() => {
    const interval = setInterval(async () => {
      pollCounter++;
      polledOdd = await axios.get("http://localhost:5000/getOddUpdate");
      if (currentPollValue !== polledOdd.data) {
        blinkOnChange(true);
        setPollingValue(polledOdd.data);
        timesOddPolledChanged++;
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [currentPollValue, blinkOnChange]);

  // Establish Socket
  useEffect(() => {
    io.emit("connection");

    io.on("oddUpdate", (odd) => {
      if (currentPushValue !== odd) {
        blinkOnChange(false);
        setPushValue(odd);
      }
    });
  }, [currentPushValue, blinkOnChange]);

  return (
    <div className="container">
      <div id="pushContainer" className="buttonLine">
        <span className="buttonLabel">Push Button</span>
        <div id="push_button" className={`betButton ${pushAnimation}`}>
          {currentPushValue}
        </div>
      </div>
      <div id="pollContainer" className="buttonLine">
        <span className="buttonLabel">Polling Button</span>
        <div id="polling_button" className={`betButton ${pollAnimation}`}>
          {currentPollValue}
        </div>
      </div>
      <div className="countersContainer">
        <span className="buttonLabel">Times Polled </span>
        <div className="counter">{pollCounter}</div>
        <span className="buttonLabel">Times Odd Changed</span>

        <div className="counter">{timesOddPolledChanged}</div>
      </div>
    </div>
  );
};
